﻿#include <iostream>
#include <vector>
#include <thread>
#include <mutex>
#include <chrono>
#include <ctime>
#include <condition_variable>
#include <random>

using namespace std;

condition_variable CV_FillVector;
bool EndConsume;

int GetRandomInRange(const int min, const int max)
{
	static default_random_engine gen(
		static_cast<unsigned>(
			chrono::system_clock::now().time_since_epoch().count())
	);
	uniform_int_distribution<int> destribution(min, max);
	return destribution(gen);
}

void Consume(int& Value, vector<int>& Vector, int neededInt, mutex& locker)
{
	unique_lock<mutex> lock(locker);
	bool bIsConsumeFill = false;

	while (!bIsConsumeFill)
	{
		CV_FillVector.wait(lock);
		if (Value < 10)
		{
			unsigned int i = 0;
			while (i < Vector.size())
			{
				if(Vector[i] == neededInt)
				{
					Value++;
					Vector.erase(Vector.begin() + i);
					i--;
					printf("Hey i found what i want - %d", neededInt);
					printf("\n");

					if (Value > 10)
					{
						bIsConsumeFill = true;
					}
				}
				i++;
			}
			Vector.clear();
		}
	}
	EndConsume = true;
}

void Produce(vector<int>& VectorToFill, mutex& locker)
{
	unique_lock<mutex> UL(locker, defer_lock);

	while (true)
	{
		this_thread::sleep_for(chrono::milliseconds(100));
		int rundNumber = GetRandomInRange(0, 10);

		UL.lock();
		VectorToFill.push_back(rundNumber);
		UL.unlock();
		if (VectorToFill.size() > 50)
		{
			CV_FillVector.notify_one();
		}
	}
}

int main()
{
	vector<int> myVector;
	vector<thread> threads;

	int SuccessConsumeCount = 0;
	mutex m;
	mutex& lockerForMyVector = m;

	for (int i = 0; i < 2; i++)
	{
		threads.push_back(thread([&]()
			{
				Produce(myVector, lockerForMyVector);
			}));
	}

	for (thread& t : threads)
	{
		t.detach();
	}
	thread ConsumeThread(Consume, ref(SuccessConsumeCount), ref(myVector), 5, ref(lockerForMyVector));

	while (!EndConsume)
	{
		printf("%d ", SuccessConsumeCount);

		this_thread::sleep_for(chrono::milliseconds(100));
	}
	printf("\n");
	printf("End work %d ", SuccessConsumeCount);
	ConsumeThread.join();
}
