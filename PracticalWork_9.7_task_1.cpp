﻿#include <iostream>
#include <vector>
#include <thread>
#include <algorithm>
#include <mutex>
#include <chrono>
#include <ctime>
#include <string>
#include <condition_variable>
#include <random>

using namespace std;

condition_variable CV_FillVector;
int CarsNum = 5;
bool EndConsume;

struct VehicleCharacteristics
{
	string VehicleName = "Error: No name";
	float EngineCapacity = 0.0f;
	int MaximumSpeed = 0;
	string VehicleColor = "Error: No color";
};

int GetRandomInRange(const int min, const int max)
{
	static default_random_engine gen(
		static_cast<unsigned>(
			chrono::system_clock::now().time_since_epoch().count())
	);
	uniform_int_distribution<int> destribution(min, max);
	return destribution(gen);
}

void Consume(vector<VehicleCharacteristics>& carsVector, mutex& locker)
{
	unique_lock<mutex> lock(locker);

	CV_FillVector.wait(lock);
	
	std::sort(carsVector.begin(), carsVector.end(), [](VehicleCharacteristics const& lhs, VehicleCharacteristics const& rhs)
		{
			if (lhs.MaximumSpeed != rhs.MaximumSpeed)
				return lhs.MaximumSpeed < rhs.MaximumSpeed;
		});

	printf("Car name	EngineCapacity	MaximumSpeed	VehicleColor");
	printf("\n");
	for (auto carInfo = carsVector.begin(); carInfo != carsVector.end(); ++carInfo)
	{
		cout << carInfo->VehicleName << "		" << carInfo->EngineCapacity << "		" << carInfo->MaximumSpeed << "		" << carInfo->VehicleColor << endl;
	}

	this_thread::sleep_for(chrono::milliseconds(20000));
}

bool CarInfoCheck(vector<VehicleCharacteristics>& carsVector)
{
	for (int i = 0; i < CarsNum; i++)
	{
		if (carsVector.at(i).EngineCapacity != 0.0f &&
			carsVector.at(i).MaximumSpeed != 0 &&
			carsVector.at(i).VehicleName != "Error: No name" &&
			carsVector.at(i).VehicleColor != "Error: No color")
		{
			return true;
		}
	}
	return false;
}

void GenerateName(vector<VehicleCharacteristics>& vectorToFill, mutex& locker)
{
	unique_lock<mutex> UL(locker, defer_lock);
	this_thread::sleep_for(chrono::milliseconds(50));

	UL.lock();
	string VehicleNamesArray[5] {"BYD", "BMW", "Chery", "Omoda", "Lada"};

	for (int i = 0; i < CarsNum; i++)
	{
		vectorToFill.at(i).VehicleName = VehicleNamesArray[GetRandomInRange(0, 4)];
	}

	if (CarInfoCheck(vectorToFill))
	{
		CV_FillVector.notify_one();
	}
	UL.unlock();
}

void GenerateEngineCapacity(vector<VehicleCharacteristics>& vectorToFill, mutex& locker)
{
	unique_lock<mutex> UL(locker, defer_lock);
	float EngineCapacity = 0.0f;
	this_thread::sleep_for(chrono::milliseconds(150));

	UL.lock();

	for (int i = 0; i < CarsNum; i++)
	{
		int tmpVal1 = GetRandomInRange(2, 12);
		int tmpVal2 = GetRandomInRange(2, 12);
		(tmpVal1 >= tmpVal2) ? EngineCapacity = tmpVal1 / tmpVal2 : EngineCapacity = tmpVal2 / tmpVal1;
		vectorToFill.at(i).EngineCapacity = EngineCapacity;
	}

	if (CarInfoCheck(vectorToFill))
	{
		CV_FillVector.notify_one();
	}
	UL.unlock();
}

void GenerateMaximumSpeed(vector<VehicleCharacteristics>& vectorToFill, mutex& locker)
{
	unique_lock<mutex> UL(locker, defer_lock);
	this_thread::sleep_for(chrono::milliseconds(110));

	UL.lock();

	for (int i = 0; i < CarsNum; i++)
	{
		vectorToFill.at(i).MaximumSpeed = GetRandomInRange(150, 320);
	}

	if (CarInfoCheck(vectorToFill))
	{
		CV_FillVector.notify_one();
	}

	UL.unlock();
}

void GenerateColor(vector<VehicleCharacteristics>& vectorToFill, mutex& locker)
{
	unique_lock<mutex> UL(locker, defer_lock);
	string VehicleNamesArray[5]{ "Green", "Red", "White", "Brown", "Black" };
	this_thread::sleep_for(chrono::milliseconds(30));

	UL.lock();

	for (int i = 0; i < CarsNum; i++)
	{
		vectorToFill.at(i).VehicleColor = VehicleNamesArray[GetRandomInRange(0, 4)];
	}

	if (CarInfoCheck(vectorToFill))
	{
		CV_FillVector.notify_one();
	}

	UL.unlock();
}

int main()
{
	vector<VehicleCharacteristics> CarsVector;
	VehicleCharacteristics CarInfo;

	for (int i = 0; i < CarsNum; i++)
	{
		CarsVector.push_back(CarInfo);
	}

	mutex m;
	mutex& lockerForMyVector = m;

	thread GenerateNameThread(GenerateName, ref(CarsVector), ref(lockerForMyVector));
	thread GenerateEngineCapacityThread(GenerateEngineCapacity, ref(CarsVector), ref(lockerForMyVector));
	thread GenerateMaximumSpeedThread(GenerateMaximumSpeed, ref(CarsVector), ref(lockerForMyVector));
	thread GenerateColorThread(GenerateColor, ref(CarsVector), ref(lockerForMyVector));

	thread ConsumeThread(Consume, ref(CarsVector), ref(lockerForMyVector));

	
	ConsumeThread.join();
	GenerateNameThread.join();
	GenerateEngineCapacityThread.join();
	GenerateMaximumSpeedThread.join();
	GenerateColorThread.join();
}
